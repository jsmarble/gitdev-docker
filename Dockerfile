FROM debian:10

RUN apt-get -qq update && \
    apt-get -qq -y install git openssh-server nano vim curl wget

COPY container_init.sh /tmp/

CMD ["/bin/bash", "/tmp/container_init.sh" ]
EXPOSE 22