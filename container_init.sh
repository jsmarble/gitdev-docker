if [ -d ~/.ssh ]; then
  echo "Already initialized"
else
  echo "Copying .ssh files"
  cp -R /ssh/ ~/.ssh/
  chown -R root:root ~/.ssh
  chmod -R 600 ~/.ssh
  chmod 700 ~/.ssh
  chmod 644 ~/.ssh/*.pub

  if [[ -v GIT_SSH_KEY ]]; then
    PRIV_KEY="/root/.ssh/$GIT_SSH_KEY"
    echo "Configuring git command with ssh key '$PRIV_KEY'"
    git config core.sshCommand "ssh -i $PRIV_KEY"
  fi

  echo "Cloning git repo from '$GIT_REPO'"
  mkdir /code
  git clone $GIT_REPO /code
  [ $? -eq 0 ]  || exit 1

  cd /code

  echo "Setting git user.name to '$GIT_USER_NAME'"
  git config --global user.name "$GIT_USER_NAME"

  echo "Setting git user.email to '$GIT_USER_EMAIL'"
  git config --global user.email $GIT_USER_EMAIL
fi

echo "Starting ssh service"
service ssh start

echo "Waiting (press Ctrl+C to exit) ..."
tail -f /dev/null  #do not stop